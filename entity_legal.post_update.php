<?php

/**
 * @file
 * Post-update scripts for Entity Legal module.
 */

declare(strict_types=1);

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\entity_legal\EntityLegalViewsConfigUpdater;
use Drupal\views\ViewEntityInterface;

/**
 * Implements hook_removed_post_updates().
 */
function entity_legal_removed_post_updates(): array {
  return [
    'entity_legal_post_update_title_pattern' => '4.0.0',
  ];
}

/**
 * Convert document version ID from string to serial (Views).
 */
function entity_legal_post_update_9002(array &$sandbox): void {
  // Cleanup state variable set in entity_legal_update_last_removed().
  // @see \entity_legal_update_last_removed()
  \Drupal::state()->delete('entity_legal.update_9002');

  /** @var \Drupal\entity_legal\EntityLegalViewsConfigUpdater $view_config_updater */
  $view_config_updater = \Drupal::classResolver(EntityLegalViewsConfigUpdater::class);
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'view', function (ViewEntityInterface $view) use ($view_config_updater): bool {
    return $view_config_updater->needsString2SerialUpdate($view);
  });
}
