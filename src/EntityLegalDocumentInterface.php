<?php

namespace Drupal\entity_legal;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an interface for entity_legal_document entities.
 */
interface EntityLegalDocumentInterface extends ConfigEntityInterface {

  /**
   * Returns the acceptance delivery method for a given user type.
   *
   * @param bool $newUser
   *   (optional) Get the method for new signups or existing accounts,
   *   defaults to FALSE.
   *
   * @return string|null
   *   The acceptance delivery method.
   */
  public function getAcceptanceDeliveryMethod(bool $newUser = FALSE): ?string;

  /**
   * Returns an acceptance form for this legal document.
   *
   * @return array
   *   The acceptance form render array.
   */
  public function getAcceptanceForm(): array;

  /**
   * Returns the label to be shown on the acceptance checkbox.
   *
   * @return string
   *   The label to be shown on the acceptance checkbox.
   */
  public function getAcceptanceLabel(): string;

  /**
   * Returns the acceptances for this entity legal document revision.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The user account to check for, or get all acceptances if NULL.
   * @param bool|null $published
   *   (optional) Get acceptances only for the currently published version,
   *   defaults to TRUE.
   *
   * @return \Drupal\entity_legal\EntityLegalDocumentAcceptanceInterface[]
   *   The acceptance entities keyed by acceptance ID.
   */
  public function getAcceptances(?AccountInterface $account = NULL, bool $published = TRUE): array;

  /**
   * Returns all versions of this legal document entity.
   *
   * @return \Drupal\entity_legal\EntityLegalDocumentVersionInterface[]
   *   All versions of this legal document entity.
   */
  public function getAllVersions(): array;

  /**
   * Returns the permission name for any user viewing this agreement.
   *
   * @return string
   *   The user permission, used with user_access.
   */
  public function getPermissionView(): string;

  /**
   * Returns the permission name for new users accepting this document.
   *
   * @return string
   *   The user permission, used with user_access.
   */
  public function getPermissionExistingUser(): string;

  /**
   * Returns the current published version of this document.
   *
   * @return \Drupal\entity_legal\EntityLegalDocumentVersionInterface|null
   *   The current legal document version or NULL if none found.
   */
  public function getPublishedVersion(): ?EntityLegalDocumentVersionInterface;

  /**
   * Returns the published document version.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentVersionInterface $versionEntity
   *   The legal document version to set as the published version.
   *
   * @return bool
   *   Whether the published version was set successfully.
   */
  public function setPublishedVersion(EntityLegalDocumentVersionInterface $versionEntity): bool;

  /**
   * Checks if the given user has agreed the current version of this document.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The user account to check, defaults to the current user.
   *
   * @return bool
   *   Whether the user has agreed to the current version.
   */
  public function userHasAgreed(?AccountInterface $account = NULL): bool;

  /**
   * Checks to see if a given user can agree to this document.
   *
   * @param bool $newUser
   *   (optional) Whether the user to check is a new user signup, defaults to
   *   FALSE.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The user account to check access permissions for. Defaults to
   *   the current user.
   *
   * @return bool
   *   Can a user agree to this document.
   */
  public function userMustAgree(bool $newUser = FALSE, ?AccountInterface $account = NULL): bool;

}
