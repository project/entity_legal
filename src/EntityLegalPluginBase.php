<?php

declare(strict_types=1);

namespace Drupal\entity_legal;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for EntityLegal plugins.
 */
abstract class EntityLegalPluginBase extends PluginBase implements EntityLegalPluginInterface {

  use DependencySerializationTrait;

  /**
   * Static cache for the legal documents that implement this plugin.
   *
   * @var \Drupal\entity_legal\EntityLegalDocumentInterface[]
   */
  protected array $documents;

  /**
   * Constructs a new plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected AccountProxyInterface $currentUser,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
    );
  }

  /**
   * Get all Entity Legal Documents for this plugin.
   *
   * @return \Drupal\entity_legal\EntityLegalDocumentInterface[]
   *   All published entity legal documents required.
   */
  public function getDocumentsForMethod(): array {
    if (!isset($this->documents)) {
      // Entity Legal administrators should never be forced to accept documents.
      if ($this->currentUser->hasPermission('administer entity legal')
        || $this->currentUser->hasPermission('bypass entity legal acceptance')) {
        $this->documents = [];
        return $this->documents;
      }

      // Ensure the correct user context for this plugin.
      if ($this->pluginDefinition['type'] === 'existing_users' && $this->currentUser->isAnonymous()) {
        $this->documents = [];
        return $this->documents;
      }

      // Get all active documents that must be agreed to.
      $properties = ['require_existing' => 1];
      if ($this->pluginDefinition['type'] === 'new_users') {
        $properties = ['require_signup' => 1];
      }
      $this->documents = $this->entityTypeManager
        ->getStorage(ENTITY_LEGAL_DOCUMENT_ENTITY_NAME)
        ->loadByProperties($properties);

      // Remove any documents from the array set that don't use the given
      // acceptance method.
      /** @var \Drupal\entity_legal\EntityLegalDocumentInterface $document */
      foreach ($this->documents as $name => $document) {
        $agreed = !$document->userMustAgree(
            $this->pluginDefinition['type'] === 'new_users'
          ) || $document->userHasAgreed();
        $isMethod = $document->getAcceptanceDeliveryMethod(
            $this->pluginDefinition['type'] === 'new_users'
        ) === $this->pluginId;

        if ($agreed || !$isMethod) {
          unset($this->documents[$name]);
        }
      }
    }
    return $this->documents;
  }

}
