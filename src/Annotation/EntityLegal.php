<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * EntityLegal plugin.
 *
 * Plugin Namespace: Plugin\EntityLegal.
 *
 * @Annotation
 */
class EntityLegal extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable name of the Entity Legal method plugin.
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The method type; "new_users" or "existing_users".
   */
  public string $type;

}
