<?php

namespace Drupal\entity_legal;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an interface defining a entity legal document version entity.
 */
interface EntityLegalDocumentVersionInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Returns the acceptances for this entity legal document version.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The user account to check for, or get all acceptances if NULL.
   *
   * @return \Drupal\entity_legal\EntityLegalDocumentAcceptanceInterface[]
   *   The acceptance entities keyed by acceptance id.
   */
  public function getAcceptances(?AccountInterface $account = NULL): array;

  /**
   * Returns the legal document version creation timestamp.
   *
   * @return int
   *   Creation timestamp of the legal document version.
   */
  public function getCreatedTime(): int;

  /**
   * Returns attached document entity.
   *
   * @return \Drupal\entity_legal\EntityLegalDocumentInterface
   *   The attached document entity.
   */
  public function getDocument(): EntityLegalDocumentInterface;

  /**
   * Returns the date for a given entity property.
   *
   * @param string $type
   *   The type of date to retrieve, updated or created.
   *
   * @return string
   *   The formatted date.
   */
  public function getFormattedDate(string $type = 'changed'): string;

  /**
   * Checks if this version is the legal document published version.
   *
   * @return bool
   *   If this id the legal document published version.
   */
  public function isPublished(): bool;

  /**
   * Publishes this version.
   *
   * @return $this
   */
  public function publish(): self;

  /**
   * Unpublishes this version.
   *
   * @return $this
   */
  public function unpublish(): self;

}
