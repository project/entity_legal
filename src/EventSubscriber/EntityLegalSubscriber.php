<?php

declare(strict_types=1);

namespace Drupal\entity_legal\EventSubscriber;

use Drupal\Component\Plugin\PluginManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Listens to kernel request event.
 */
class EntityLegalSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new event subscriber instance.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $entityLegalPluginManager
   *   The plugin manager.
   */
  public function __construct(protected PluginManagerInterface $entityLegalPluginManager) {}

  /**
   * Listens to kernel request event.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function checkRedirect(RequestEvent $event): void {
    $context = ['event' => $event];

    // Execute Redirect method plugin.
    $this->entityLegalPluginManager->createInstance('redirect')->execute($context);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['checkRedirect', 28];
    return $events;
  }

}
