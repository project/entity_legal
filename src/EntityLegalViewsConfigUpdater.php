<?php

declare(strict_types=1);

namespace Drupal\entity_legal;

use Drupal\views\ViewEntityInterface;

/**
 * Provides a BC layer views providing old configurations.
 *
 * This is heavily inspired from \Drupal\views\ViewsConfigUpdater.
 *
 * @see \Drupal\views\ViewsConfigUpdater
 * @see \views_view_presave()
 */
class EntityLegalViewsConfigUpdater {

  /**
   * Performs all required updates.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The View to update.
   *
   * @return bool
   *   Whether the view was updated.
   */
  public function updateAll(ViewEntityInterface $view): bool {
    return $this->processDisplayHandlers($view, FALSE, function (&$handler, $handler_type, $key, $display_id) {
      $changed = FALSE;
      if ($this->processString2SerialUpdateHandler($handler, $handler_type)) {
        $changed = TRUE;
      }
      return $changed;
    });
  }

  /**
   * Processes all display handlers.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The View to update.
   * @param bool $returnOnChanged
   *   Whether processing should stop after a change is detected.
   * @param callable $handlerProcessor
   *   A callback performing the actual update.
   *
   * @return bool
   *   Whether the view was updated.
   */
  protected function processDisplayHandlers(ViewEntityInterface $view, $returnOnChanged, callable $handlerProcessor): bool {
    $changed = FALSE;
    $displays = $view->get('display');
    $handlerTypes = ['field', 'argument', 'sort', 'relationship', 'filter'];

    foreach ($displays as $displayId => &$display) {
      foreach ($handlerTypes as $handlerType) {
        $handlerTypePlural = $handlerType . 's';
        if (!empty($display['display_options'][$handlerTypePlural])) {
          foreach ($display['display_options'][$handlerTypePlural] as $key => &$handler) {
            if ($handlerProcessor($handler, $handlerType, $key, $displayId)) {
              $changed = TRUE;
              if ($returnOnChanged) {
                return $changed;
              }
            }
          }
        }
      }
    }

    if ($changed) {
      $view->set('display', $displays);
    }

    return $changed;
  }

  /**
   * Updates the timestamp fields settings by adding time diff and tooltip.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The View to update.
   *
   * @return bool
   *   Whether the view was updated.
   */
  public function needsString2SerialUpdate(ViewEntityInterface $view): bool {
    return $this->processDisplayHandlers($view, FALSE, function (array &$handler, string $handlerType): bool {
      return $this->processString2SerialUpdateHandler($handler, $handlerType);
    });
  }

  /**
   * Processes timestamp fields settings by adding time diff and tooltip.
   *
   * @param array $handler
   *   A display handler.
   * @param string $handlerType
   *   The handler type.
   *
   * @return bool
   *   Whether the handler was updated.
   */
  protected function processString2SerialUpdateHandler(array &$handler, string $handlerType): bool {
    $entityTypes = [
      'entity_legal_document_version' => 'name',
      'entity_legal_document_acceptance' => 'document_version_name',
    ];
    if (
      isset($handler['entity_type'])
      && isset($entityTypes[$handler['entity_type']])
      && isset($handler['entity_field'])
      && (
        $handler['entity_field'] === $entityTypes[$handler['entity_type']]
        || $handler['entity_field'] === 'vid'
      )
    ) {
      $handler['entity_field'] = 'vid';
      $handler['field'] = 'vid';
      return TRUE;
    }
    return FALSE;
  }

}
