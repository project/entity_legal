<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Controller;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Utility\Token;
use Drupal\entity_legal\EntityLegalDocumentInterface;
use Drupal\entity_legal\EntityLegalDocumentVersionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides various controllers for module routes.
 */
class EntityLegalController extends ControllerBase {

  /**
   * Constructs a new controller instance.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(protected Token $token) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('token'));
  }

  /**
   * Provides a title callback for the legal document entity edit form.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentInterface $entity_legal_document
   *   The legal document entity.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The title.
   */
  public function documentEditPageTitle(EntityLegalDocumentInterface $entity_legal_document): MarkupInterface {
    return $this->t('Edit %label', ['%label' => $entity_legal_document->label()]);
  }

  /**
   * Provides a page callback for the legal document entity.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentInterface $entity_legal_document
   *   The legal document entity.
   * @param \Drupal\entity_legal\EntityLegalDocumentVersionInterface|null $entity_legal_document_version
   *   (optional) The legal document version entity, defaults to the current
   *   published version of this document.
   *
   * @return array
   *   The render array.
   */
  public function documentPage(EntityLegalDocumentInterface $entity_legal_document, ?EntityLegalDocumentVersionInterface $entity_legal_document_version = NULL): array {
    if (is_null($entity_legal_document_version)) {
      $entity_legal_document_version = $entity_legal_document->getPublishedVersion();
      if (!$entity_legal_document_version) {
        throw new NotFoundHttpException();
      }
    }

    // If the specified version is unpublished, display a message.
    if ($entity_legal_document_version->id() != $entity_legal_document->getPublishedVersion()->id()) {
      $this->messenger()->addMessage('You are viewing an unpublished version of this legal document.', 'warning');
    }

    return $this->entityTypeManager()
      ->getViewBuilder(ENTITY_LEGAL_DOCUMENT_VERSION_ENTITY_NAME)
      ->view($entity_legal_document_version);
  }

  /**
   * Provides a title callback for the legal document entity.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentInterface $entity_legal_document
   *   The legal document entity.
   */
  public function documentPageTitle(EntityLegalDocumentInterface $entity_legal_document): string {
    $pattern = $entity_legal_document->get('settings')['title_pattern'];
    return $this->token->replace($pattern, [
      ENTITY_LEGAL_DOCUMENT_ENTITY_NAME => $entity_legal_document,
    ]);
  }

  /**
   * Provides a page callback for the legal document entity Version form.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentInterface $entity_legal_document
   *   The legal document entity.
   *
   * @return array
   *   A form render array.
   */
  public function documentVersionForm(EntityLegalDocumentInterface $entity_legal_document): array {
    $entity_legal_document_version = $this->entityTypeManager()
      ->getStorage(ENTITY_LEGAL_DOCUMENT_VERSION_ENTITY_NAME)
      ->create([
        'document_name' => $entity_legal_document->id(),
      ]);
    return $this->entityFormBuilder()->getForm($entity_legal_document_version);
  }

  /**
   * Provides a title callback for the legal document entity Version add form.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentInterface $entity_legal_document
   *   The legal document entity.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The page title.
   */
  public function documentVersionAddFormTitle(EntityLegalDocumentInterface $entity_legal_document): MarkupInterface {
    return $this->t('Add %type legal document version', ['%type' => $entity_legal_document->label()]);
  }

  /**
   * Provides a title callback for the legal document version edit form.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentVersionInterface $entity_legal_document_version
   *   The legal document version entity.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The page title.
   */
  public function documentVersionEditFormTitle(EntityLegalDocumentVersionInterface $entity_legal_document_version): MarkupInterface {
    return $this->t('Edit %label', ['%label' => $entity_legal_document_version->label()]);
  }

}
