<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\entity_legal\EntityLegalDocumentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form for deleting a custom block entity.
 */
class EntityLegalDocumentAcceptanceForm extends FormBase {

  /**
   * The Entity Legal Document used by this form.
   */
  protected EntityLegalDocumentInterface $document;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private temp store factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    protected PrivateTempStoreFactory $privateTempStoreFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Sets the legal document.
   *
   * @param \Drupal\entity_legal\EntityLegalDocumentInterface $document
   *   The legal document.
   */
  public function setDocument(EntityLegalDocumentInterface $document): void {
    $this->document = $document;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_legal_document_acceptance_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $has_agreed = $this->document->userHasAgreed();

    $form['agree'] = [
      '#title' => $this->document->getAcceptanceLabel(),
      '#type' => 'checkbox',
      '#required' => TRUE,
      '#default_value' => $has_agreed,
      '#disabled' => $has_agreed,
    ];

    $form['submit'] = [
      '#value' => t('Submit'),
      '#type' => 'submit',
      '#access' => !$has_agreed,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $published_version = $this->document->getPublishedVersion();
    $this->entityTypeManager
      ->getStorage(ENTITY_LEGAL_DOCUMENT_ACCEPTANCE_ENTITY_NAME)
      ->create(['vid' => $published_version->id()])
      ->save();

    $tempStore = $this->privateTempStoreFactory->get('entity_legal');

    // Restore potential postponed messages and show them on the correct page.
    // @see \Drupal\entity_legal\Plugin\EntityLegal\Redirect::execute()
    if ($grouped_messages = $tempStore->get('postponed_messages')) {
      foreach ($grouped_messages as $type => $messages) {
        foreach ($messages as $message) {
          $this->messenger()->addMessage($message, $type);
        }
      }
      $tempStore->delete('postponed_messages');
    }
  }

}
