<?php

namespace Drupal\entity_legal;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface ResponsiveMenusInterface.
 */
interface EntityLegalPluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Execute callback for Entity Legal method plugin.
   *
   * @param array $context
   *   Contextual information for plugin to execute on.
   */
  public function execute(array &$context = []);

}
