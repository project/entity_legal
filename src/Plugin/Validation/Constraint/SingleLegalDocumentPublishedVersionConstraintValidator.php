<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_legal\Entity\EntityLegalDocumentVersion;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Provides a validator for the SingleLegalDocumentPublishedVersion constraint.
 */
class SingleLegalDocumentPublishedVersionConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Constructs a new constraint validator instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {
    /** @var \Drupal\Core\Field\FieldItemListInterface $value */
    if ($value->isEmpty()) {
      return;
    }

    $published = $value->value;
    if (!$published) {
      // Don't validate anything if the FALSE has been set.
      return;
    }

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $value->getEntity();
    $entityTypeId = $entity->getEntityTypeId();
    $entityId = $entity->id();

    if ($value->getFieldDefinition()->getName() !== 'published' || $entityTypeId !== 'entity_legal_document_version') {
      // The constraint has been set on the wrong field.
      throw new \Exception("The SingleLegalDocumentPublishedVersion constraint cannot be set on other field than 'published' of 'entity_legal_document_version' entity type.");
    }

    $query = $this->entityTypeManager->getStorage($entityTypeId)->getQuery()->accessCheck(FALSE);
    // Using isset() instead of !empty() as 0 and '0' are valid ID values for
    // entity types using string IDs.
    if (isset($entityId)) {
      $idKey = $entity->getEntityType()->getKey('id');
      $query->condition($idKey, $entityId, '<>');
    }

    $ids = $query
      ->condition('published', TRUE)
      ->condition('document_name', $entity->bundle())
      ->range(0, 1)
      ->execute();

    if ($ids) {
      $id = reset($ids);
      $publishedVersion = EntityLegalDocumentVersion::load($id);
      $legalDocument = $publishedVersion->get('document_name')->entity->label();
      $this->context->addViolation($constraint->message, [
        '%legal_document' => $legalDocument,
        '%version' => $publishedVersion->label(),
      ]);
    }
  }

}
