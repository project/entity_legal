<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal entity legal document migration source plugin.
 *
 * @MigrateSource(
 *   id = "entity_legal_document",
 *   source_module = "entity_legal",
 * )
 */
class EntityLegalDocument extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select('entity_legal_document', 'd')->fields('d');
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'name' => $this->t('The legal document ID.'),
      'label' => $this->t('The human-readable label of the legal document.'),
      'require_signup' => $this->t('Require new users to accept this document on signup.'),
      'require_existing' => $this->t('Require existing users to accept this document.'),
      'settings' => $this->t('An array of additional data related to the legal document.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $ids['name']['type'] = 'string';
    return $ids;
  }

}
