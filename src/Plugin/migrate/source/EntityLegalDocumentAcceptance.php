<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;

/**
 * Drupal entity legal document acceptance migration source plugin.
 *
 * @MigrateSource(
 *   id = "entity_legal_document_acceptance",
 *   source_module = "entity_legal",
 * )
 */
class EntityLegalDocumentAcceptance extends FieldableEntity {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select('entity_legal_document_acceptance', 'a')->fields('a');
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'aid' => $this->t('The entity ID of this agreement.'),
      'document_version_name' => $this->t('The name of the document version this acceptance is bound to.'),
      'uid' => $this->t('The author of the acceptance.'),
      'acceptance_date' => $this->t('The date the document was accepted.'),
      'data' => $this->t('A dump of user data to help verify acceptances.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $ids['aid']['type'] = 'integer';
    return $ids;
  }

}
