<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Plugin\EntityLegal;

use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_legal\EntityLegalPluginBase;

/**
 * Method class for alerting existing users via Drupal set message.
 *
 * @EntityLegal(
 *   id = "message",
 *   label = @Translation("Drupal warning message, prompting the user, until accepted"),
 *   type = "existing_users",
 * )
 */
class Message extends EntityLegalPluginBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(array &$context = []): void {
    foreach ($this->getDocumentsForMethod() as $document) {
      // cspell:disable-next-line
      $message = $this->t('Please accept the @document_name.', [
        '@document_name' => Link::createFromRoute($document->getPublishedVersion()->label(), 'entity.entity_legal_document.canonical', [
          'entity_legal_document' => $document->id(),
        ])->toString(),
      ]);

      $this->messenger()->addWarning($message);
    }
  }

}
