<?php

declare(strict_types=1);

namespace Drupal\entity_legal\Plugin\EntityLegal;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Method class for displaying a checkbox on the user register form.
 *
 * @EntityLegal(
 *   id = "form_inline",
 *   label = @Translation("Checkbox on signup form with embedded document"),
 *   type = "new_users",
 * )
 */
class ProfileFormEmbedded extends ProfileForm {

  /**
   * Constructs a new plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected AccountProxyInterface $currentUser,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $this->entityTypeManager, $this->currentUser);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute(array &$context = []): void {
    parent::execute($context);

    if (!empty($this->getDocumentsForMethod())) {
      $viewBuilder = $this->entityTypeManager
        ->getViewBuilder(ENTITY_LEGAL_DOCUMENT_VERSION_ENTITY_NAME);

      foreach ($this->getDocumentsForMethod() as $document) {
        $documentMarkup = $viewBuilder->view($document->getPublishedVersion());
        $context['form']["legal_{$document->id()}"]['#prefix'] = DeprecationHelper::backwardsCompatibleCall(
          \Drupal::VERSION, '10.3.0',
          fn () => $this->renderer->renderInIsolation($documentMarkup),
          fn () => $this->renderer->renderPlain($documentMarkup),
        );
      }
    }
  }

}
