<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_legal\Functional;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Tests update scripts.
 *
 * @group entity_legal
 */
class UpdateTest extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles(): void {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-10.3.0.bare.standard.php.gz',
      __DIR__ . '/../../fixtures/update/install_entity_legal.php',
      __DIR__ . '/../../fixtures/update/legal_documents.php',
      __DIR__ . '/../../fixtures/update/update_9002.php',
    ];
  }

  /**
   * Tests entity_legal_update_9002() & entity_legal_post_update_9002().
   *
   * @see \entity_legal_update_9002()
   * @see \entity_legal_post_update_9002()
   */
  public function testUpdate9002(): void {
    $db = \Drupal::database();
    $schema = $db->schema();
    $configFactory = \Drupal::configFactory();
    $trail = 'display.default.display_options';

    $this->assertTrue($schema->fieldExists('entity_legal_document_version', 'name'));
    $id = $db->query("SELECT name FROM {entity_legal_document_version}")->fetchField();
    $this->assertSame('legal_notice_1683794048', $id);

    $this->assertTrue($schema->fieldExists('entity_legal_document_version_data', 'name'));
    $id = $db->query("SELECT name FROM {entity_legal_document_version_data}")->fetchField();
    $this->assertSame('legal_notice_1683794048', $id);

    $id = $db->query("SELECT entity_id FROM {entity_legal_document_version__3054549535}")->fetchField();
    $this->assertSame('legal_notice_1683794048', $id);

    $this->assertTrue($schema->fieldExists('entity_legal_document_acceptance', 'document_version_name'));
    $ids = $db->query("SELECT document_version_name FROM {entity_legal_document_acceptance}")->fetchCol();
    $this->assertSame(['legal_notice_1683794048', 'legal_notice_1683794048'], $ids);

    $config = $configFactory->get('views.view.legal_document_acceptances');
    $this->assertSame('document_version_name', $config->get("$trail.fields.document_version_name.field"));
    $this->assertSame('document_version_name', $config->get("$trail.fields.document_version_name.entity_field"));
    $this->assertSame('document_version_name', $config->get("$trail.relationships.document_version_name.field"));
    $this->assertSame('document_version_name', $config->get("$trail.relationships.document_version_name.entity_field"));

    $this->runUpdates();

    $this->assertFalse($schema->fieldExists('entity_legal_document_version', 'name'));
    $this->assertTrue($schema->fieldExists('entity_legal_document_version', 'vid'));
    $id = $db->query("SELECT vid FROM {entity_legal_document_version}")->fetchField();
    $this->assertEquals(1683794048, $id);

    $this->assertFalse($schema->fieldExists('entity_legal_document_version_data', 'name'));
    $this->assertTrue($schema->fieldExists('entity_legal_document_version_data', 'vid'));
    $id = $db->query("SELECT vid FROM {entity_legal_document_version_data}")->fetchField();
    $this->assertEquals(1683794048, $id);

    $id = $db->query("SELECT entity_id FROM {entity_legal_document_version__3054549535}")->fetchField();
    $this->assertEquals(1683794048, $id);

    $this->assertFalse($schema->fieldExists('entity_legal_document_acceptance', 'document_version_name'));
    $this->assertTrue($schema->fieldExists('entity_legal_document_acceptance', 'vid'));
    $ids = $db->query("SELECT vid FROM {entity_legal_document_acceptance}")->fetchCol();
    $this->assertEquals([1683794048, 1683794048], $ids);

    $config = $configFactory->get('views.view.legal_document_acceptances');
    $this->assertSame('vid', $config->get("$trail.fields.document_version_name.field"));
    $this->assertSame('vid', $config->get("$trail.fields.document_version_name.entity_field"));
    $this->assertSame('vid', $config->get("$trail.relationships.document_version_name.field"));
    $this->assertSame('vid', $config->get("$trail.relationships.document_version_name.entity_field"));
  }

}
