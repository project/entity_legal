<?php

/**
 * @file
 * Fixture file for https://www.drupal.org/project/entity_legal/issues/3163562.
 *
 * @see https://www.drupal.org/project/entity_legal/issues/3163562
 * @see \entity_legal_update_9002()
 * @see \entity_legal_post_update_9002()
 */

use Drupal\Core\Database\Database;
use Drupal\Core\Serialization\Yaml;

$db = Database::getConnection();

$db->insert('entity_legal_document_version')->fields([
  'name' => 'legal_notice_1683794048',
  'document_name' => 'legal_notice',
  'uuid' => 'd09583f6-27b2-499f-aeeb-7b59221e604f',
  'langcode' => 'en',
])->execute();

$db->insert('entity_legal_document_version_data')->fields([
  'name' => 'legal_notice_1683794048',
  'document_name' => 'legal_notice',
  'langcode' => 'en',
  'published' => '1',
  'label' => 'Version 1.0',
  'acceptance_label' => 'I agree to the <a href="[entity_legal_document:url]">Legal notice</a> document',
  'created' => '1683794060',
  'changed' => '1683794060',
  'default_langcode' => '1',
])->execute();

$db->insert('entity_legal_document_version__3054549535')->fields([
  'bundle' => 'legal_notice',
  'deleted' => '0',
  'entity_id' => 'legal_notice_1683794048',
  'revision_id' => 'legal_notice_1683794048',
  'langcode' => 'en',
  'delta' => '0',
  'entity_legal_document_text_value' => '<p>You shall obey!</p>',
  'entity_legal_document_text_summary' => '',
  'entity_legal_document_text_format' => 'basic_html',
])->execute();

$db->insert('entity_legal_document_acceptance')->fields([
  'aid' => '1',
  'document_version_name' => 'legal_notice_1683794048',
  'uid' => '1',
  'acceptance_date' => '1683794166',
])->execute();

$db->insert('entity_legal_document_acceptance')->fields([
  'aid' => '2',
  'document_version_name' => 'legal_notice_1683794048',
  'uid' => '2',
  'acceptance_date' => '1683795000',
])->execute();

$db->insert('config')->fields([
  'collection' => '',
  'name' => 'views.view.legal_document_acceptances',
  'data' => serialize(Yaml::decode(file_get_contents(__DIR__ . '/update_9002.views.view.legal_document_acceptances.yml'))),
])->execute();
