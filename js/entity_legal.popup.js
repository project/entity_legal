/**
 * @file
 * Drupal behaviors for Entity Legal popup acceptance method.
 */

(($) => {
  /**
   * Modal popup for Entity Legal module.
   *
   * Executed using core jQuery ui dialog with modifications to prevent user
   * from closing the popup.
   */
  Drupal.behaviors.entityLegalPopup = {
    attach(context, settings) {
      $.each(settings.entityLegalPopup, (index, value) => {
        const $popup = $('<div />').html(value.popupContent);
        $popup.dialog({
          autoOpen: true,
          closeOnEscape: false,
          modal: true,
          draggable: false,
          title: value.popupTitle,
          width: '80%',
          beforeClose() {
            // Prevent closing of modal window.
            return false;
          },
          open() {
            // Remove close button from titlebar when opening.
            $popup
              .closest('.ui-dialog')
              .find('.ui-dialog-titlebar-close')
              .remove();
          },
        });
      });
    },
  };
})(jQuery);
